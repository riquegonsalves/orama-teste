
(function() {
'use strict';


  // list all rependencies within the page + controler if needed
  angular.module('MyHomeModule', ['ngLodash'])

  .value("config", {
    baseUrl: "https://s3.amazonaws.com/orama-media/json/fund_detail_full.json?limit=1000&offset=0&serializer=fund_detail_full"
  })

  .factory('FundsFactory', function($http, $q, config, lodash){

    var _getFunds = function (filtersFunds) {
      return $http.get(config.baseUrl)
        .then(function(response) {
          return $q(function(resolve, reject){

            var allFunds = response.data;

            console.log(allFunds);
            
            var minApplication = filtersFunds.minApplication;
            var timeRescue = filtersFunds.timeRescue;
            var term = filtersFunds.term;
            var risk = filtersFunds.risk;
            var selectedClasses = filtersFunds.selectedClasses;

            var classes = [];

            var filtered = lodash.filter(allFunds, function(application) {
              // console.log('app', application);

              var minApplicationCriteria = minApplication > parseInt(application.operability.minimum_initial_application_amount);

              var riskCriteria = risk === 0 ? true : risk >= application.specification.fund_risk_profile.score_range_order;

              var searchCriteria;
              if(!term || term.length < 3) {
                searchCriteria = true;
              } else {
                var name = application.full_name.toLowerCase();
                var searchTerm = term.toLowerCase();
                searchCriteria = name.indexOf(searchTerm) > -1;
              }


              var retrievalCriteria = timeRescue > parseInt(application.operability.retrieval_liquidation_days);
              

              return minApplicationCriteria && riskCriteria && searchCriteria && retrievalCriteria;
              
              
            });            

            var groupedByType = lodash.groupBy(filtered, 'specification.fund_type');
            var keysFounded = lodash.keys(groupedByType);
            var fundGroupByClass = lodash.map(keysFounded, function(key) {
              var typeValues = lodash.get(groupedByType, key);	
              return {
                name_type: key,
                values: lodash.groupBy(typeValues, 'specification.fund_class')
              };
            
            });

            var funds = { funds: response.data, groupedByType: groupedByType, fundGroupByClass: fundGroupByClass };
            resolve(funds);
          });
        });
    };

    var _getClasses = function () {
      return $http.get(config.baseUrl)
        .then(function(response) {
          return $q(function(resolve, reject){

            var allFunds = response.data;
            var groupedByType = lodash.groupBy(allFunds, 'specification.fund_type');
            var typesFounded = lodash.keys(groupedByType);
            var types = lodash.map(typesFounded, function(key) {

              var typeValues = lodash.get(groupedByType, key);	
              var groupByClass = lodash.groupBy(typeValues, 'specification.fund_class');
              var classesFounded = lodash.keys(groupByClass);

              var classes = lodash.map(classesFounded, function(c) {
                return {
                  name: c,
                  selected: true
                };
              });
              
              return {
                name: key,
                selected: true,
                classes: classes
              };
            
            });

            resolve(types);
          });
        });
    };
  
    return {
      getFunds: _getFunds,
      getClasses: _getClasses
    };

  })

  // Injection are added automatically with ng-anotate
 .controller('MyHomeController', function ($scope, $stateParams, $state, $controller, $http, FundsFactory, lodash, $timeout) {
 
    // add some usefull code here. Note than Angular2 does not have controler anymore
    //  building your code directly within directive could be a good idea. 
    $scope.model = 0;
    $scope.minApplication = 1000000;
    $scope.timeRescue = 33;
    $scope.riskFilter = 0;

    var _timeout;

   

    $scope.getFunds = function() {
      if(_timeout) { // if there is already a timeout in process cancel it
        $timeout.cancel(_timeout);
      }
      _timeout = $timeout(function() {
          _timeout = null;

          var sidebar = $scope.sidebar;
          var classes = [];

          lodash.each(sidebar, function(type){
            lodash.each(type.classes, function(c){
              classes.push(c);
            });
          });

          $scope.selectedClasses = lodash.filter(classes, function(c) {
            return c.selected === true;
          });

          var filtersApplications = {
            minApplication: $scope.minApplication,
            timeRescue: $scope.timeRescue,
            term: $scope.searchTerm,
            risk: $scope.riskFilter,
            selectedClasses: $scope.selectedClasses
          };
    
          FundsFactory.getFunds(filtersApplications)
            .then(function(funds) {
              $scope.types = funds.fundGroupByClass;
          });
      }, 500);
    };

    $scope.riskSelect = function(num) {
      if ($scope.riskFilter == num) {
        $scope.riskFilter = 0;
      } else {
        $scope.riskFilter = num;
      }
      $scope.getFunds();
    };
    
    $scope.riskClass = function(application) {
      var riskIndex = application.specification.fund_risk_profile.score_range_order;
      var className = '';

      if (!application.is_active) {
        className = 'disabled';
      }

      className += ' risk-' + riskIndex;
      return className;
    };

    $scope.toggleSidebarSubMenu = function(e) {
      var category_li = $(e.target).parent().parent();
      category_li.toggleClass('opened');
    };

    $scope.selectType = function(type) {
      lodash.each(type.classes, function(c) {
        c.selected = type.selected ? true : false;
      });

      $scope.getFunds();
    };

    $scope.selectClass = function(key, checked) {
      $scope.getFunds();
    };

    $scope.showType = function(key) {
      var find = lodash.find($scope.sidebar, function(type) {
        return lodash.isEqual(type.name.toLowerCase(), key.toLowerCase());
      });


      if (lodash.isObject(find)) {
        return find.selected ? true : false;
      } 
    };

    $scope.showClass = function(key) {
      var find = lodash.find($scope.selectedClasses, function(c) {
        
        return lodash.isEqual(c.name.toLowerCase(), key.toLowerCase());
      });



      if (lodash.isObject(find)) {
        return true;
      } else {
        return false;
      }
    };

    $scope.initSidebar = function() {
      FundsFactory.getClasses()
            .then(function(types) {
              $scope.sidebar = types;

              $scope.getFunds();
          });
    };

    $scope.initSidebar(); 

    $scope.getFunds();
  });


  
  console.log ("HomeModule Loaded");
})();